#include "seriell.h"

#include "glob.h"
#include "hwi.h"
#include "timer.h"
#include "calculations.h"
#include "leds.h"
#include "measure.h"
#include "str.h"

int _TARA = 0;
int _NETTO = 0;
reset_t _RESET = NO_RESET;
measure_t _MEASURE = NO_MEASURING;
coin_t _ACTIVE_COIN = TEN_CENT;
select_coin_t _SELECT_COIN_STATE = CHOOSE_COINS;

StructTC *tcbase4 = TCB4_BASE;
StructTC *tcbase5 = TCB5_BASE;

/**
 * @brief Get the name of a coin (coin_t)
 *
 * @param coin The coin for which the string should be returned
 * @param buf A char* buffer to save the string value. The max size should be #MAX_COIN_SIZE_STR.
 */
void get_coin_name(coin_t coin, char *buf)
{
    switch (coin)
    {
    case ONE_CENT:
        strcpy_mps(buf, "1 Cent");
        break;
    case TWO_CENT:
        strcpy_mps(buf, "2 Cent");
        break;
    case FIVE_CENT:
        strcpy_mps(buf, "5 Cent");
        break;
    case TEN_CENT:
        strcpy_mps(buf, "10 Cent");
        break;
    case TWENTY_CENT:
        strcpy_mps(buf, "20 Cent");
        break;
    case FIFTY_CENT:
        strcpy_mps(buf, "50 Cent");
        break;
    case ONE_EURO:
        strcpy_mps(buf, "1 Euro");
        break;
    case TWO_EURO:
        strcpy_mps(buf, "2 Euro");
        break;
    }
}

/**
 * @brief Get the weight of a coin (coin_t)
 *
 * @param coin The coin of which the weight should be returned
 * @return float The weight of the coin
 */
float get_coin_weight(coin_t coin)
{
    switch (coin)
    {
    case ONE_CENT:
        return 2.3;
    case TWO_CENT:
        return 3.06;
    case FIVE_CENT:
        return 3.92;
    case TEN_CENT:
        return 4.10;
    case TWENTY_CENT:
        return 5.74;
    case FIFTY_CENT:
        return 7.80;
    case ONE_EURO:
        return 7.50;
    case TWO_EURO:
        return 8.5;
    }
}

/**
 * @brief Prints a line break to the serial interface
 *
 */
void line_break()
{
    // LF
    while (!putch(0xa))
        ;

    // CR
    while (!putch(0xd))
        ;
}

/**
 * @brief Just prints a start header
 *
 */
void write_welcome_message()
{
    line_break();
    puts("#############################");
    line_break();
    puts("\tMuenzzaehler");
    line_break();
    puts("#############################");
    line_break();
}

/**
 * @brief Writes the value of the glob _NETTO state to the serial com.
 *
 * Also shows the current coin count via the LEDs
 *
 */
void write_mass()
{
    puts("Current State: ");

    char netto_str[MAX_ITOA_SIZE];
    clear_str(netto_str, MAX_ITOA_SIZE);
    char *z = itoa(_NETTO, netto_str + (MAX_ITOA_SIZE - 1));
    puts(z);
    line_break();

    output_to_leds(calc_coin_count(_NETTO, get_coin_weight(_ACTIVE_COIN)));
}

/**
 * @brief Resets the scale global values to be able to start a new calculation.
 *
 * All global flag values are set to their zero (default) value
 *
 */
void reset_scale()
{
    _TARA = 0;
    _NETTO = 0;
    _MEASURE = NO_MEASURING;
    _RESET = NO_RESET;
    _SELECT_COIN_STATE = CHOOSE_COINS;
}

/**
 * @brief Prints the result of the measure proccess to the serial com.
 *
 * It outputs the Tara value, the net value, the gross value and the coin count
 *
 */
void print_scale_result()
{
    puts("Gewicht der Muenzen: \n");
    line_break();

    char output_str[MAX_ITOA_SIZE];

    clear_str(output_str, MAX_ITOA_SIZE);
    char *z = itoa(_TARA, output_str + (MAX_ITOA_SIZE - 1));
    puts("\tTara:\t\t");
    puts(z);
    line_break();

    clear_str(output_str, MAX_ITOA_SIZE);
    z = itoa(_NETTO, output_str + (MAX_ITOA_SIZE - 1));
    puts("\tNetto:\t\t");
    puts(z);
    line_break();

    clear_str(output_str, MAX_ITOA_SIZE);
    z = itoa(_TARA + _NETTO, output_str + (MAX_ITOA_SIZE - 1));
    puts("\tBrutto:\t\t");
    puts(z);
    line_break();

    int coin_cnt = calc_coin_count(_NETTO, get_coin_weight(_ACTIVE_COIN));
    clear_str(output_str, MAX_ITOA_SIZE);
    z = itoa(coin_cnt, output_str + (MAX_ITOA_SIZE - 1));
    puts("\tCoin count:\t");
    puts(z);
    line_break();
}

/**
 * @brief Prints a menu which shows which coin a user can select with which key
 *
 */
void print_coin_menu()
{
    puts("<1> 1 Cent");
    line_break();
    puts("<2> 2 Cent ");
    line_break();
    puts("<3> 5 Cent");
    line_break();
    puts("<4> 10 Cent ");
    line_break();
    puts("<5> 20 Cent");
    line_break();
    puts("<6> 50 Cent ");
    line_break();
    puts("<7> 1 Euro");
    line_break();
    puts("<8> 2 Euro");
    line_break();
}

/**
 * @brief Selects a coin from a taken user input
 *
 * @param c The index of the coin as char
 * @return int If the coin was set successfully 1 otherwise 0
 */
int select_coin(char c)
{
    switch (c)
    {
    case '1':
        _ACTIVE_COIN = ONE_CENT;
        return 1;
    case '2':
        _ACTIVE_COIN = TWO_CENT;
        return 1;
    case '3':
        _ACTIVE_COIN = FIVE_CENT;
        return 1;
    case '4':
        _ACTIVE_COIN = TEN_CENT;
        return 1;
    case '5':
        _ACTIVE_COIN = TWENTY_CENT;
        return 1;
    case '6':
        _ACTIVE_COIN = FIFTY_CENT;
        return 1;
    case '7':
        _ACTIVE_COIN = ONE_EURO;
        return 1;
    case '8':
        _ACTIVE_COIN = TWO_EURO;
        return 1;
    default:
        return 0;
    }
}

/**
 * @brief Just prints scale and user info
 *
 */
void print_info()
{
    puts("Es kann folgende Muenze gewogen werden: ");

    char active_coin_name[MAX_COIN_SIZE_STR];
    clear_str(active_coin_name, MAX_COIN_SIZE_STR);
    get_coin_name(_ACTIVE_COIN, active_coin_name);
    puts(active_coin_name);
    line_break();
    line_break();

    puts("Info:");
    line_break();
    puts("\tControlls: ");
    line_break();
    puts("\t\tTaste SW1 - Tara des Gefaesses");
    line_break();
    puts("\t\tTaste SW2 - Starten des Wiegens von Muenzen");
    line_break();
    puts("\t\tTaste SW3 - Ausgabe");
    line_break();
}

/**
 * @brief The main application loop and entrypoint
 *
 * @return int The exit status of the application. Should be 0 if everything went ok.
 */
int main()
{
    StructPMC *pmcbase = PMC_BASE; // Basisadresse des Power Management controllers
    pmcbase->PMC_PCER = 0x06C00;   // Clock fuer PIOA, PIOB und Timer4, einschalten; Um T5 zu aktivieren =0x06400 ->0x06C00

    // LEDs initialisieren
    init_leds();

    // Hardware interrupt initialisieren
    init_hwi();

    // Serielle Schnittstelle initialisieren
    init_ser();

    // Timer initialisieren
    init_timer();

    // Default reset the scale in not initialized mode
    _RESET = RESET;
    int isInit = 1;

    while (1)
    {
        // If reset flag is set
        if (_RESET == RESET)
        {
            if (!isInit)
            {
                puts("Wiege-Vorgang beendet");
                line_break();
                print_scale_result();
                line_break();
                puts("Press any key...");
                line_break();
                while (!getch())
                    ;
            }
            else
            {
                isInit = 0;
            }

            reset_scale();
            write_welcome_message();
            continue;
        }

        // If the user should choose a coin
        if (_SELECT_COIN_STATE == CHOOSE_COINS)
        {

            int success = 0;
            print_coin_menu();
            char c;

            // Wait for a correct input for a coin
            while (success == 0)
            {
                while (!(c = getch()))
                    ;
                success = select_coin(c);

                if (success == 0)
                {
                    puts("Falsche eingabe: ");
                    while (!putch(c))
                        ;
                    line_break();
                }
            }

            _SELECT_COIN_STATE = COIN_SELECTED;
            print_info();
            continue;
        }

        if (_MEASURE == TAKE_TARA)
        {

            _MEASURE = NO_MEASURING;

            // Measure of the current weight and save as tare

            int measure_result = take_measurement();

            if (measure_result == -1)
            {
                puts("Error while taking measurement for Tara.");
                line_break();
                return measure_result;
            }

            _TARA = measure_result;
            puts("Gemessener Tara Wert: ");

            char tara_str[MAX_ITOA_SIZE];
            clear_str(tara_str, MAX_ITOA_SIZE);
            char *z = itoa(measure_result, tara_str + (MAX_ITOA_SIZE - 1));
            puts(z);
            line_break();
        }
        else if (_MEASURE == TAKE_MEASURES)
        {
            puts("Es koennen nun Muenzen gewogen werden");
            line_break();
            while (_MEASURE == TAKE_MEASURES && _RESET != RESET)
            {
                int measure_result = take_measurement();

                if (measure_result == -1)
                {
                    puts("Error while taking measurement.");
                    line_break();
                    return;
                }

                if (measure_result - _TARA != _NETTO)
                {
                    _NETTO = measure_result - _TARA;
                    write_mass();
                }
            }
        }
    }

    return 0;
}
