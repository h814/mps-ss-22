#ifndef MEASURE_H
#define MEASURE_H

#include "glob.h"

#define MEASURE_COUNT 20

float take_measurement();

#endif
