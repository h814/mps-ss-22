#ifndef CALCULATIONS_H
#define CALCULATIONS_H

#include "glob.h"

float calculate_mass();
int calc_coin_count(int mass, float coin_weight);

#endif
