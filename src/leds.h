#ifndef LEDS_H
#define LEDS_H

#include "../h/pio.h"

static StructPIO *piobaseB = PIOB_BASE; //!< Basisadresse der PIOB

void init_leds();
void turn_off_leds();
void output_to_leds(int n);

#endif