#include "leds.h"

/**
 * @brief Init the leds for the board
 *
 */
void init_leds()
{
    piobaseB->PIO_PER = ALL_LEDS;  // Enable LEDs
    piobaseB->PIO_OER = ALL_LEDS;  // Set LEDs to output
    piobaseB->PIO_SODR = ALL_LEDS; // turn LEDs off
}

/**
 * @brief Turns of all leds
 *
 */
void turn_off_leds()
{
    piobaseB->PIO_SODR = ALL_LEDS; // turn LEDs off
}

/**
 * @brief Shows a number in binary with the onboard LEDs.
 *
 * Only eight LEDs are available so it is required, that the number is smaller than 255.
 * Otherwise the number will not be displayed correctly.
 *
 * @param n The value to show with the LEDs
 */
void output_to_leds(int n)
{
    turn_off_leds();

    if (0x1 & n)
    {
        piobaseB->PIO_CODR = LED8;
    }

    if (0x2 & n)
    {
        piobaseB->PIO_CODR = LED7;
    }

    if (0x4 & n)
    {
        piobaseB->PIO_CODR = LED6;
    }

    if (0x8 & n)
    {
        piobaseB->PIO_CODR = LED5;
    }

    if (0x10 & n)
    {
        piobaseB->PIO_CODR = LED4;
    }

    if (0x20 & n)
    {
        piobaseB->PIO_CODR = LED3;
    }

    if (0x40 & n)
    {
        piobaseB->PIO_CODR = LED2;
    }

    if (0x80 & n)
    {
        piobaseB->PIO_CODR = LED1;
    }
}
