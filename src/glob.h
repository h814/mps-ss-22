#ifndef GLOB_H
#define GLOB_H

#include "types.h"
#include "../h/tc.h"

#define MAX_COIN_SIZE_STR 7 //!< Max size of a coin type string

extern int _TARA;                        //!< Global flag for the current Tara value
extern int _NETTO;                       //!< Global flag for the current net value
extern reset_t _RESET;                   //!< Global flag for indicating a reset.
extern measure_t _MEASURE;               //!< Global flag if continuous measurements should be taken
extern coin_t _ACTIVE_COIN;              //!< Global flag which coin is set up to be calculated
extern select_coin_t _SELECT_COIN_STATE; //!< Global flag which indicates if a coin was set by the user

extern StructTC *tcbase4; //!< Global pointer to timer base 4, so we don't need to redefine in every method where we use it
extern StructTC *tcbase5; //!< Global pointer to timer base 5, so we don't need to redefine in every method where we use it

#endif
