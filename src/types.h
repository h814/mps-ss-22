#ifndef TYPES_H
#define TYPES_H

/**
 * @brief Reset indicator
 *
 */
typedef enum
{
    NO_RESET = 0,
    RESET = 1,
} reset_t;

/**
 * @brief Type to indicate the measure state.
 *
 */
typedef enum
{
    NO_MEASURING = 0,  //!< Device should not take any continuos measurements. Single measures can be done individually.
    TAKE_MEASURES = 1, //!< Device should take continuos measurements
    TAKE_TARA = 2,     //!< Device should get tara value
} measure_t;

/**
 * @brief All measurable coins
 *
 */
typedef enum
{
    ONE_CENT = 0,
    TWO_CENT = 1,
    FIVE_CENT = 2,
    TEN_CENT = 3,
    TWENTY_CENT = 4,
    FIFTY_CENT = 5,
    ONE_EURO = 6,
    TWO_EURO = 7
} coin_t;

/**
 * @brief Indicator type if a user should select a coin
 *
 */
typedef enum
{
    CHOOSE_COINS = 0,
    COIN_SELECTED = 1
} select_coin_t;

#endif
