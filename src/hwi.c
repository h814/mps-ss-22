#include "hwi.h"

/**
 * @brief Interrupt handler
 *
 * Handles the KEY1 to KEY3 presses.
 * Sets the global flags for actions depending on the pressed key
 *
 */
void taste_irq_handler(void)
{

    StructPIO *piobaseB = PIOB_BASE; // Basisadresse der PIOB
    StructAIC *aicbase = AIC_BASE;

    if (!(piobaseB->PIO_PDSR & KEY1))
    {
        _MEASURE = TAKE_TARA;
    }

    if (!(piobaseB->PIO_PDSR & KEY2))
    {
        // KEY2 pressed
        _MEASURE = TAKE_MEASURES;
    }

    if (!(piobaseB->PIO_PDSR & KEY3))
    {
        _RESET = RESET;
    }

    // Reset Interupt
    aicbase->AIC_EOICR = piobaseB->PIO_ISR;
}

/**
 * @brief Inits the hardware interrupt for the board
 *
 * Sets taste_irq_handler as interrupt handler
 *
 */
void init_hwi()
{
    StructPIO *piobaseB = PIOB_BASE; // Basisadresse der PIOB
    StructAIC *aicbase = AIC_BASE;   // Basisadress der Advanced Interupt controllers

    piobaseB->PIO_PER = KEY1 | KEY2 | KEY3; // Enable  keys

    // Interrupts für PIOB deaktivieren
    aicbase->AIC_IDCR = 1 << 14;

    // Alle interrupts clearen
    aicbase->AIC_ICCR = 1 << 14;

    // Interrupt handler zuweisen
    aicbase->AIC_SVR[PIOB_ID] = (unsigned int)taste_irq_handler;

    // Priorität des interrupts setzen
    aicbase->AIC_SMR[PIOB_ID] = 0x7;
    // piobaseB->PIO_ISR;

    // Reenable interrupts
    aicbase->AIC_IECR = 1 << 14;

    // Reset Interrupt
    aicbase->AIC_EOICR = piobaseB->PIO_ISR;

    // Enable interrupts for key1 and key2
    piobaseB->PIO_IER = KEY1 | KEY2 | KEY3;
}
