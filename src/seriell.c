#include "seriell.h"

#define DEFAULT_BAUD 38400
#define CLOCK_SPEED 25000000
#define CD 0x29 //!< CD = (CLOCK_SPEED / (16*(DEFAULT_BAUD))	// 25MHz / ( 16 * 38400) = 40.69  -> 41 -> 0x29

/**
 * @brief Initializing of the serial interface USART0
 *
 * @return int Everytime 0
 */
int init_ser()
{
	StructPIO *piobaseA = PIOA_BASE;
	StructPMC *pmcbase = PMC_BASE;
	StructUSART *usartbase0 = USART0;

	pmcbase->PMC_PCER = 0x4;	 // Clock f�r US0 einschalten
	piobaseA->PIO_PDR = 0x18000; // US0 TxD und RxD
	usartbase0->US_CR = 0xa0;	 // TxD und RxD disable
	usartbase0->US_BRGR = CD;	 // Baud Rate Generator Register
	usartbase0->US_MR = 0x8c0;	 // Keine Parit�t, 8 Bit, MCKI
	usartbase0->US_CR = 0x50;	 // TxD und RxD enable

	return 0;
}

/**
 * @brief Writes a single character to the serial interface
 *
 * @param Zeichen A character to write to the serial interface
 * @return char The written character or 0 if nothing was written
 */
char putch(char Zeichen)
{
	StructUSART *usartbase0 = USART0;

	if (usartbase0->US_CSR & US_TXRDY)
	{
		usartbase0->US_THR = Zeichen;
	}
	else
	{
		Zeichen = 0; // wenn keine Ausgabe
	}
	return Zeichen;
}

/**
 * @brief Gets a character of the serial interface
 *
 * @return char The read character or 0 if nothing was read
 */
char getch(void)
{
	StructUSART *usartbase0 = USART0;
	char Zeichen;

	if (usartbase0->US_CSR & US_RXRDY)
	{
		Zeichen = usartbase0->US_RHR;
	}
	else
	{
		Zeichen = 0;
	}

	return Zeichen;
}

/**
 * @brief Writes a string to the serial interface
 *
 * Also replaces 0xA with 0xA and 0xD
 *
 * @param String The string to write
 * @return int The number of written characters
 */
int puts(char *String)
{
	int i = 0;

	while (String[i] != 0)
	{
		while (!(putch(String[i])))
			;
		i = i + 1;

		if (String[i - 1] == 0xa)
		{
			while (!(putch(0xd)))
				;
			i = i + 1; // Ist das so wichtig?
		}
	}
	return i;
}
