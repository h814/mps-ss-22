#include "measure.h"
#include "timer.h"
#include "calculations.h"

/**
 * @brief Takes #MEASURE_COUNT measures and calculates the average of it.
 *
 * @return float The measured value or -1 if something went wrong
 */
float take_measurement()
{
    int loaded_b_4 = 0;
    int loaded_b_5 = 0;

    int mass_cnt = 0;
    long mass_calc = 0;

    tcbase4->TC_CCR = TC_SWTRG;
    tcbase5->TC_CCR = TC_SWTRG;

    int measurement = 0;

    while (mass_cnt <= MEASURE_COUNT)
    {
        if (!loaded_b_4)
        {
            loaded_b_4 = tcbase4->TC_SR & 0x40;
        }

        if (!loaded_b_5)
        {
            loaded_b_5 = tcbase5->TC_SR & 0x40;
        }

        if (loaded_b_4 && loaded_b_5)
        {
            float mass = calculate_mass();
            tcbase4->TC_CCR = TC_SWTRG;
            tcbase5->TC_CCR = TC_SWTRG;
            loaded_b_4 = loaded_b_5 = 0;
	    mass_cnt += 1;
		       
	    if(mass_cnt == 1) continue;
	    
            mass_calc += mass;
        }

        if (mass_cnt >= MEASURE_COUNT)
        {
            mass_calc /= (MEASURE_COUNT - 1);
            return mass_calc;
        }
    }

    return -1;
}
