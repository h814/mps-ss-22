#include "calculations.h"

/**
 * @brief Calculates the mass of two period durations.
 *
 * Requires the Timer 4 and Timer 5 to have captured values to determine the period duration
 *
 * @return float The mass calculated of the timer values
 */
float calculate_mass()
{
	// Sim values
	//static const int C1 = 2000;
	//static const int C2 = 0;

	// Real values
	static const int C1 = 18039;
	static const int C2 = 40;

	volatile int captureRA1 = tcbase4->TC_RA;
	volatile int captureRB1 = tcbase4->TC_RB;
	volatile int capturediff1 = captureRB1 - captureRA1;
	volatile float Periodendauer1 = capturediff1 / 12.5;

	captureRA1 = tcbase5->TC_RA;
	captureRB1 = tcbase5->TC_RB;
	capturediff1 = captureRB1 - captureRA1;
	volatile float Periodendauer1_5 = capturediff1 / 12.5;

	return C1 * ((Periodendauer1 / Periodendauer1_5) - 1) - C2;
}

/**
 * @brief Calculates the count of coins by their mass.
 *
 * @param mass The mass of all coins
 * @param coin_weight The mass of a single coin
 * @return int The count of the coins calculated from the mass
 */
int calc_coin_count(int mass, float coin_weight)
{
	if (mass <= 0)
		return 0;
	else
		return mass / coin_weight;
}
