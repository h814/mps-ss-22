#ifndef HWI_H
#define HWI_H

#include "../h/pio.h"
#include "../h/aic.h"
#include "../h/tc.h"

#include "glob.h"

void taste_irq_handler(void) __attribute__((interrupt));
void init_hwi();

#endif