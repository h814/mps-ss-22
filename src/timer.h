#ifndef TIMER_H
#define TIMER_H

#include "../h/pio.h"
#include "../h/tc.h"
#include "../h/pmc.h"

#include "glob.h"

#define TC_INIT TC_CLKS_MCK2 | TC_LDBSTOP | TC_CAPT | TC_LDRA_RISING_EDGE | TC_LDRB_RISING_EDGE //!< Macro for the timer configuration

void init_timer();

#endif