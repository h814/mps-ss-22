#include "str.h"
#include "limits.h"

/**
 * @brief Parses an integer to its value as string
 *
 * @param i The integer value which should be parsed to a string
 * @param ptr_to_str A pointer to the end of the char array
 * @return char* The start value of the string in the array
 */
char *itoa(int i, char *ptr_to_str)
{
	int isMin = 0;
	int isSigned = 0;

	*ptr_to_str = '\0';
	--ptr_to_str;

	if (i == 0)
	{
		*ptr_to_str = '0';
		return ptr_to_str;
	}

	unsigned int tmp = 1;
	if (i == INT_MIN)
	{
		isSigned = 1;
		tmp = i;
		isMin = 1;
	}

	if (i < 0 && !isMin)
	{
		isSigned = 1;
		i = -i;
	}

	while (i != 0 && tmp != 0)
	{

		if (isMin)
		{
			*ptr_to_str = (tmp % 10) + '0';
			--ptr_to_str;
			tmp /= 10;
		}
		else
		{
			*ptr_to_str = (i % 10) + '0';
			--ptr_to_str;
			i /= 10;
		}
	}

	if (isSigned > 0)
	{
		*ptr_to_str = '-';
	}
	else
		++ptr_to_str;

	return ptr_to_str;
}

/**
 * @brief Clears a c string
 *
 * @param str A pointer to the start of the string
 * @param len The length of the string
 */
void clear_str(char *str, int len)
{
	int i;
	for (i = 0; i < len; ++i)
	{
		*(str + i) = '\0';
	}
}

/**
 * @brief Copies a string from a source buffer to a destination buffer
 *
 * @param d The destination buffer
 * @param s The source buffer
 * @return char* The start of the copied string
 */
char *strcpy_mps(char *d, const char *s)
{
	char *saved = d;
	while (*s)
	{
		*d++ = *s++;
	}
	*d = 0;
	return saved;
}
