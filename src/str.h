#ifndef STR_H
#define STR_H

#define MAX_ITOA_SIZE 12 //!< Max size for a int to string conversion char array

char *itoa(int i, char *ptr_to_str);
void clear_str(char *str, int len);
char *strcpy_mps(char *d, const char *s);

#endif
