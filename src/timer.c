#include "timer.h"

/**
 * @brief Inits the timer
 *
 */
void init_timer()
{
    // Periodendauer einer der von der Waage gelieferten Frequenz messen
    // Die Waage liefert ca. 16kHz entspricht einer Periodendauer von 62,5us
    // durch den Vorteiler von 32 ergibt sich an TIOA4 eine Periodendauer von ca. 2ms
    StructPIO *piobaseA = PIOA_BASE;

    piobaseA->PIO_PDR = 0x090;
    tcbase4->TC_CCR = TC_CLKDIS;
    tcbase4->TC_CMR = TC_INIT;
    tcbase4->TC_CCR = TC_CLKEN;
    tcbase4->TC_CCR = TC_SWTRG;

    tcbase5->TC_CCR = TC_CLKDIS;
    tcbase5->TC_CMR = TC_INIT;
    tcbase5->TC_CCR = TC_CLKEN;
    tcbase5->TC_CCR = TC_SWTRG;
}
