/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "Coin counter MPS 2022", "index.html", [
    [ "Coin counter MPS 2022 Documentation", "index.html", [
      [ "Description", "index.html#autotoc_md1", null ],
      [ "Tasks", "index.html#autotoc_md2", [
        [ "Task 1", "index.html#autotoc_md3", null ],
        [ "Task 2", "index.html#autotoc_md4", null ],
        [ "Task 3", "index.html#autotoc_md5", null ],
        [ "Task 4", "index.html#autotoc_md6", null ],
        [ "Task 5", "index.html#autotoc_md7", null ],
        [ "Task 6", "index.html#autotoc_md8", null ],
        [ "Task 7", "index.html#autotoc_md9", null ]
      ] ],
      [ "Documentation", "index.html#autotoc_md10", null ],
      [ "Install instructions", "index.html#autotoc_md11", [
        [ "minicom configuration", "index.html#autotoc_md12", null ],
        [ "Program execution", "index.html#program_execution", null ]
      ] ],
      [ "User manual", "index.html#autotoc_md13", null ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ],
      [ "Globals", "globals.html", [
        [ "All", "globals.html", null ],
        [ "Functions", "globals_func.html", null ],
        [ "Variables", "globals_vars.html", null ],
        [ "Enumerations", "globals_enum.html", null ],
        [ "Enumerator", "globals_eval.html", null ],
        [ "Macros", "globals_defs.html", null ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"Muenzzaehler_8c.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';