var types_8h =
[
    [ "coin_t", "types_8h.html#aa284f5dd03c172bf5683c69db33ff508", [
      [ "ONE_CENT", "types_8h.html#aa284f5dd03c172bf5683c69db33ff508acbd7a1be0133fcc88a2ee09dde067dbe", null ],
      [ "TWO_CENT", "types_8h.html#aa284f5dd03c172bf5683c69db33ff508afd37ed02707ba00e29a5349523fc5cc5", null ],
      [ "FIVE_CENT", "types_8h.html#aa284f5dd03c172bf5683c69db33ff508aace8021fa6326c6296418ef7dc930780", null ],
      [ "TEN_CENT", "types_8h.html#aa284f5dd03c172bf5683c69db33ff508a050e637ec2a8186e850bfc769925022e", null ],
      [ "TWENTY_CENT", "types_8h.html#aa284f5dd03c172bf5683c69db33ff508a258b0a95fd7a191b42fec3ae4cf8ad34", null ],
      [ "FIFTY_CENT", "types_8h.html#aa284f5dd03c172bf5683c69db33ff508a8f8526345224b8a17382650c28085663", null ],
      [ "ONE_EURO", "types_8h.html#aa284f5dd03c172bf5683c69db33ff508ac1bfab712c638af4505b2308068ca742", null ],
      [ "TWO_EURO", "types_8h.html#aa284f5dd03c172bf5683c69db33ff508a1442fdcac4a510d59b77f118de27a95c", null ]
    ] ],
    [ "measure_t", "types_8h.html#a0845d513b38892f5ed6a0aa2b4c3e40b", [
      [ "NO_MEASURING", "types_8h.html#a0845d513b38892f5ed6a0aa2b4c3e40bac4c78e63a60b12f7da4a57f00825bb01", null ],
      [ "TAKE_MEASURES", "types_8h.html#a0845d513b38892f5ed6a0aa2b4c3e40baa68df9729f10e36e9a04d92923ddd4d2", null ],
      [ "TAKE_TARA", "types_8h.html#a0845d513b38892f5ed6a0aa2b4c3e40ba02e80ce2c20693d6e690f5a2f3b0ea43", null ]
    ] ],
    [ "reset_t", "types_8h.html#ae7c43560e6c8192e9dfc2d7291beb6d4", [
      [ "NO_RESET", "types_8h.html#ae7c43560e6c8192e9dfc2d7291beb6d4a8c3db29efacd5f2db4773dbe028cffb5", null ],
      [ "RESET", "types_8h.html#ae7c43560e6c8192e9dfc2d7291beb6d4a589b7d94a3d91d145720e2fed0eb3a05", null ]
    ] ],
    [ "select_coin_t", "types_8h.html#a7089bdddbc217c08ade0b8969cbc0aed", [
      [ "CHOOSE_COINS", "types_8h.html#a7089bdddbc217c08ade0b8969cbc0aedab64350a1830b5584ab109959652727d3", null ],
      [ "COIN_SELECTED", "types_8h.html#a7089bdddbc217c08ade0b8969cbc0aeda114f2d0c88bf168f3e650e14ba07abb7", null ]
    ] ]
];