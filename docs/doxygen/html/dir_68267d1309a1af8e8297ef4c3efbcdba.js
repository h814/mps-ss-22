var dir_68267d1309a1af8e8297ef4c3efbcdba =
[
    [ "calculations.c", "calculations_8c.html", "calculations_8c" ],
    [ "calculations.h", "calculations_8h.html", "calculations_8h" ],
    [ "glob.h", "glob_8h.html", "glob_8h" ],
    [ "hwi.c", "hwi_8c.html", "hwi_8c" ],
    [ "hwi.h", "hwi_8h.html", "hwi_8h" ],
    [ "leds.c", "leds_8c.html", "leds_8c" ],
    [ "leds.h", "leds_8h.html", "leds_8h" ],
    [ "measure.c", "measure_8c.html", "measure_8c" ],
    [ "measure.h", "measure_8h.html", "measure_8h" ],
    [ "Muenzzaehler.c", "Muenzzaehler_8c.html", "Muenzzaehler_8c" ],
    [ "seriell.c", "seriell_8c.html", "seriell_8c" ],
    [ "seriell.h", "seriell_8h.html", "seriell_8h" ],
    [ "str.c", "str_8c.html", "str_8c" ],
    [ "str.h", "str_8h.html", "str_8h" ],
    [ "timer.c", "timer_8c.html", "timer_8c" ],
    [ "timer.h", "timer_8h.html", "timer_8h" ],
    [ "types.h", "types_8h.html", "types_8h" ]
];