var indexSectionsWithContent =
{
  0: "_cdfghilmnoprstw",
  1: "cghlmrst",
  2: "cgilmoprstw",
  3: "_t",
  4: "cmrs",
  5: "cfnort",
  6: "cdmt",
  7: "c"
};

var indexSectionNames =
{
  0: "all",
  1: "files",
  2: "functions",
  3: "variables",
  4: "enums",
  5: "enumvalues",
  6: "defines",
  7: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Files",
  2: "Functions",
  3: "Variables",
  4: "Enumerations",
  5: "Enumerator",
  6: "Macros",
  7: "Pages"
};

