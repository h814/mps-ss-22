var searchData=
[
  ['calc_5fcoin_5fcount_6',['calc_coin_count',['../calculations_8c.html#a303b42a278ced808500cf2f37d3da3f9',1,'calc_coin_count(int mass, float coin_weight):&#160;calculations.c'],['../calculations_8h.html#a303b42a278ced808500cf2f37d3da3f9',1,'calc_coin_count(int mass, float coin_weight):&#160;calculations.c']]],
  ['calculate_5fmass_7',['calculate_mass',['../calculations_8c.html#a14fd6ce74b045a938e8b7da07988f339',1,'calculate_mass():&#160;calculations.c'],['../calculations_8h.html#a14fd6ce74b045a938e8b7da07988f339',1,'calculate_mass():&#160;calculations.c']]],
  ['calculations_2ec_8',['calculations.c',['../calculations_8c.html',1,'']]],
  ['calculations_2eh_9',['calculations.h',['../calculations_8h.html',1,'']]],
  ['cd_10',['CD',['../seriell_8c.html#a1050140a3d78b059f809a424e0d9e1c7',1,'seriell.c']]],
  ['choose_5fcoins_11',['CHOOSE_COINS',['../types_8h.html#a7089bdddbc217c08ade0b8969cbc0aedab64350a1830b5584ab109959652727d3',1,'types.h']]],
  ['clear_5fstr_12',['clear_str',['../str_8c.html#ad9c3a1969d0479fac4f9bd415f182430',1,'clear_str(char *str, int len):&#160;str.c'],['../str_8h.html#ad9c3a1969d0479fac4f9bd415f182430',1,'clear_str(char *str, int len):&#160;str.c']]],
  ['clock_5fspeed_13',['CLOCK_SPEED',['../seriell_8c.html#a548dc96417e4a056d5c31abeec9a8408',1,'seriell.c']]],
  ['coin_20counter_20mps_202022_20documentation_14',['Coin counter MPS 2022 Documentation',['../index.html',1,'']]],
  ['coin_5fselected_15',['COIN_SELECTED',['../types_8h.html#a7089bdddbc217c08ade0b8969cbc0aeda114f2d0c88bf168f3e650e14ba07abb7',1,'types.h']]],
  ['coin_5ft_16',['coin_t',['../types_8h.html#aa284f5dd03c172bf5683c69db33ff508',1,'types.h']]]
];
