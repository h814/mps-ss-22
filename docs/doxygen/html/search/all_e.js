var searchData=
[
  ['take_5fmeasurement_63',['take_measurement',['../measure_8c.html#ab6eed6c3a317e0c202c5be2224e6a22f',1,'take_measurement():&#160;measure.c'],['../measure_8h.html#ab6eed6c3a317e0c202c5be2224e6a22f',1,'take_measurement():&#160;measure.c']]],
  ['take_5fmeasures_64',['TAKE_MEASURES',['../types_8h.html#a0845d513b38892f5ed6a0aa2b4c3e40baa68df9729f10e36e9a04d92923ddd4d2',1,'types.h']]],
  ['take_5ftara_65',['TAKE_TARA',['../types_8h.html#a0845d513b38892f5ed6a0aa2b4c3e40ba02e80ce2c20693d6e690f5a2f3b0ea43',1,'types.h']]],
  ['taste_5firq_5fhandler_66',['taste_irq_handler',['../hwi_8c.html#ad86e6c45b9f9b3dc4030e6a1a0351709',1,'taste_irq_handler(void):&#160;hwi.c'],['../hwi_8h.html#a834feaf46ec2d988501e3e8fed0c60ca',1,'taste_irq_handler(void) __attribute__((interrupt)):&#160;hwi.c']]],
  ['tc_5finit_67',['TC_INIT',['../timer_8h.html#a3fe2d6f6d0f04fdc411d5af3476ef0c3',1,'timer.h']]],
  ['tcbase4_68',['tcbase4',['../glob_8h.html#aa0d9f15e12348d2d0ef0d0c047648246',1,'tcbase4():&#160;Muenzzaehler.c'],['../Muenzzaehler_8c.html#aa0d9f15e12348d2d0ef0d0c047648246',1,'tcbase4():&#160;Muenzzaehler.c']]],
  ['tcbase5_69',['tcbase5',['../glob_8h.html#a47583a3350f795d6f311463deaf2d8d1',1,'tcbase5():&#160;Muenzzaehler.c'],['../Muenzzaehler_8c.html#a47583a3350f795d6f311463deaf2d8d1',1,'tcbase5():&#160;Muenzzaehler.c']]],
  ['ten_5fcent_70',['TEN_CENT',['../types_8h.html#aa284f5dd03c172bf5683c69db33ff508a050e637ec2a8186e850bfc769925022e',1,'types.h']]],
  ['timer_2ec_71',['timer.c',['../timer_8c.html',1,'']]],
  ['timer_2eh_72',['timer.h',['../timer_8h.html',1,'']]],
  ['turn_5foff_5fleds_73',['turn_off_leds',['../leds_8c.html#a3140b552461b93b90c0419fb7ad7b495',1,'turn_off_leds():&#160;leds.c'],['../leds_8h.html#a3140b552461b93b90c0419fb7ad7b495',1,'turn_off_leds():&#160;leds.c']]],
  ['twenty_5fcent_74',['TWENTY_CENT',['../types_8h.html#aa284f5dd03c172bf5683c69db33ff508a258b0a95fd7a191b42fec3ae4cf8ad34',1,'types.h']]],
  ['two_5fcent_75',['TWO_CENT',['../types_8h.html#aa284f5dd03c172bf5683c69db33ff508afd37ed02707ba00e29a5349523fc5cc5',1,'types.h']]],
  ['two_5feuro_76',['TWO_EURO',['../types_8h.html#aa284f5dd03c172bf5683c69db33ff508a1442fdcac4a510d59b77f118de27a95c',1,'types.h']]],
  ['types_2eh_77',['types.h',['../types_8h.html',1,'']]]
];
