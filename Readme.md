# Coin counter MPS 2022 Documentation

[TOC]

## Description

This coin counter is able to measure the number of equal coins on a scale and output the result in the console as well as at the LEDs of the board (in binary). Only coins of the same type can be measured. At the beginning, a coin container can be placed on the scale, the weight of it will be excluded from the measurement.

## Tasks

Here are the task we should solve in this project

### Task 1

Initialization of the required periphery.

### Task 2

Issue greeting and information (e.g. which coins) on the terminal.

### Task 3

Weighing, taring and displaying on the terminal of the tare (empty vessel weight), after placing a vessel for the coins and pressing the SW1 key. 

### Task 4

Output information on the terminal that coins can now be weighed after pressing the SW2 key. 

### Task 5

Weighing and displaying the net weight (weight of coins) on the terminal. Display the number of coins in binary on the 8 LEDs of the board.

### Task 6

After pressing the SW3 key, the tare, net and gross (tare plus net) values in grams and the number of weighed coins are to be output on the terminal. Then continue with task 2 with a new weighing process.

### Task 7

- Document your solution
- Provide detailed functional descriptions (see protocols date1 to date5).
- Supply installation instructions.
- Supply a user manual.
- Sell your solution to the appropriate lab supervisor (stick to the specifications if possible).
- Be prepared to answer some questions from the appropriate caregiver.
- Look to be able to respond to small change requests.

## Documentation

The code documentation is placed after the initial description and instructions

## Install instructions

### minicom configuration

1. Open a new terminal and type `minicom`
2. Press *CTRL+A* and then *Z*
3. Press *P*
4. Set the baud rate to 38400, the parity to "none" and the bits to 8 (key combination "D", "L" and "V").

![minicom configruation example](/images/minicom_settings.png)

### Program execution {#program_execution}

1. Navigate to the project folder
2. Open a new terminal inside the project folder <br/>
![open terminal](/images/open_terminal.png)
3. Open snavigator with the command `snavigator`
4. If no project for Termin6 exists click on *New Project* and choose the project folder as root and choose Termin6 as name for the *.proj* file
5. Now click on the project Termin6 <br/>
![choose project](/images/choose_project.png)
6. Open the Build Tools under *Tools > Build*
7. Click on Build
8. Now the project is ready to debug. (You need to click on Debug)
9. In the debug menu select the file Muenzzaehler.elf as program and continue with pressing Ok
10. Open the target settings under *File > Target-Settings*
11. In the *Target Selection* configure the target to match the boards settings <br/>
![target settings](/images/target_settings.png)
12. If a message shows up that says *Connected to target* you are good to go. Otherwise connect to the target manually
13. Now use the debug control tools to run the program <br/>
![debug tools](/images/debugger.png)

## User manual

1. Start the program and minicom. Use the section [Program execution](@ref program_execution) to configure everything.
2. Select the coin you want to count
3. Place you coin container on the scale
4. Follow the instructions on the minicom screen
    1. Press Key 1 to take the tare value (it should be outputted on the screen)
    2. Press Key 2 to start the measuring
    3. Put the coins into the container (you will see the progress)
    4. Press Key 3 to stop the measure task and print the result
5. You can restart the measure process now
